R Getting and Cleaning Data
===========================


This repo contains the R code I wrote to complete the course "Getting and
Cleaning Data". This code is organized in 4 Jupyter notebooks (one for each
quiz):


1. [Week 1 Quiz](https://gitlab.com/mwoitek/r-getting-and-cleaning-data/-/blob/master/week_1/week_1_quiz.ipynb)
1. [Week 2 Quiz](https://gitlab.com/mwoitek/r-getting-and-cleaning-data/-/blob/master/week_2/week_2_quiz.ipynb)
1. [Week 3 Quiz](https://gitlab.com/mwoitek/r-getting-and-cleaning-data/-/blob/master/week_3/week_3_quiz.ipynb)
1. [Week 4 Quiz](https://gitlab.com/mwoitek/r-getting-and-cleaning-data/-/blob/master/week_4/week_4_quiz.ipynb)
